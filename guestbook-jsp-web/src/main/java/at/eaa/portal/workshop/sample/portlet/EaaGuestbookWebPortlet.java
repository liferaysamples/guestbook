package at.eaa.portal.workshop.sample.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import at.eaa.portal.workshop.sample.constants.EaaGuestbookWebPortletKeys;
import at.eaa.portal.workshop.sample.dto.EntryJspDto;
import guestbook.app.model.Entry;
import guestbook.app.service.EntryLocalService;

/**
 * Guestbook portlet class.
 * 
 * @author Emrah Bozkaya
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=EaaGuestbookWeb", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + EaaGuestbookWebPortletKeys.EAAGUESTBOOKWEB,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class EaaGuestbookWebPortlet extends MVCPortlet {

	private static Log log = LogFactoryUtil.getLog(EaaGuestbookWebPortlet.class.getName());

	@Reference
	private EntryLocalService entryLocalService;

	public void addEntry(ActionRequest request, ActionResponse response) {

		String name = ParamUtil.getString(request, "name");
		String email = ParamUtil.getString(request, "email");
		String message = ParamUtil.getString(request, "message");
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		ServiceContext serviceContext;
		try {
			serviceContext = ServiceContextFactory.getInstance(request);
			this.entryLocalService.addEntry(themeDisplay.getUserId(), name, email, message, serviceContext);
		} catch (PortalException e) {
			log.error("Error during persisting new guestbook entry!", e);
		}
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		
		log.info("### render");
		
		ServiceContext serviceContext = null;
		try {
			serviceContext = ServiceContextFactory.getInstance(renderRequest);
			long groupId = serviceContext.getScopeGroupId();
			
			List<Entry> entries = this.entryLocalService.getEntries(groupId);
			List<EntryJspDto> jspEntries = new ArrayList<EntryJspDto>();
			
			for (Entry entry : entries) {
				log.info("### " + entry.getMessage());
				jspEntries.add(new EntryJspDto(entry.getName(), entry.getUserName(), entry.getMessage()));
			}
			
			renderRequest.setAttribute("entries", jspEntries);
		} catch (PortalException e) {
			log.error("Error during retrieving guestbook entries!", e);
		}		
		
		super.render(renderRequest, renderResponse);
	}
}

package at.eaa.portal.workshop.sample.constants;

/**
 * @author NBOEMR
 */
public class EaaGuestbookWebPortletKeys {

	public static final String EAAGUESTBOOKWEB =
		"at_eaa_portal_workshop_sample_EaaGuestbookWebPortlet";

}
<%@page import="java.util.List"%>
<%@ include file="/init.jsp" %>

<portlet:renderURL var="addEntryURL">
    <portlet:param name="mvcPath" value="/edit_entry.jsp"></portlet:param>
</portlet:renderURL>

<%-- <jsp:useBean id="entries" class="java.util.ArrayList" scope="request"/> --%>

<% List entries = (List) request.getAttribute("entries"); %>

<liferay-ui:search-container>
    <liferay-ui:search-container-results results="<%= entries %>" />

    <liferay-ui:search-container-row
        className="at.eaa.portal.workshop.sample.dto.EntryJspDto"
        modelVar="entry"
    >
        <liferay-ui:search-container-column-text property="message" />

        <liferay-ui:search-container-column-text property="name" />
        
        <liferay-ui:search-container-column-text property="email" />        
    </liferay-ui:search-container-row>

    <liferay-ui:search-iterator />
</liferay-ui:search-container>

<aui:button-row>
    <aui:button onClick="<%= addEntryURL.toString() %>" value="Add Entry"></aui:button>
</aui:button-row>
create index IX_E35C3FCA on workshop_Entry (groupId);
create index IX_16463AD4 on workshop_Entry (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_34076656 on workshop_Entry (uuid_[$COLUMN_LENGTH:75$], groupId);
import React from 'react';
import ReactDOM from 'react-dom';

// Home Page
const HomePage = () => (
	<div>
	  <h1>Home</h1>
	  <p>
		  Version 1.0
	  </p>
	</div>
);

export default HomePage;
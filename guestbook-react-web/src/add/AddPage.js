import React from 'react';
import ReactDOM from 'react-dom';

const AddPage = (props) => {
  return (
	  <div>
		  <form onSubmit={props.saveData}>
			  <div>
				<div className="form-group">
					<label htmlFor="exampleInputEmail1">Email Adresse</label>
					<input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
				</div>
				<div className="form-group">
					<label htmlFor="name">Name</label>
					<input type="text" className="form-control" id="name" placeholder="Name" />
				</div>
				<div className="form-check">
					<label className="form-check-label" htmlFor="message">Nachricht</label>
					<textarea rows="4" cols="50" name="message" form="form-check-label"></textarea>
				</div>
				<button type="submit" className="btn btn-primary">Speichern</button>
			</div>
		  </form>
	  </div>
  )
}

export default AddPage;
import React from 'react';
import ReactDOM from 'react-dom';

import { HashRouter as Router } from 'react-router-dom';
import { Route, Link } from 'react-router-dom';

import Menu from './Menu';
import HomePage from './home/HomePage';
import ViewEntriesPage from './view/ViewEntriesPage';
import AddPage from './add/AddPage';

export default class extends React.Component {
	constructor() {
		super();
		this.state = this.getState(
			[
				{"name":"Test test", "email": "test@liferay.com", "message": "Dies ist eine Testnachricht"},
				{"name":"Test1 test", "email": "test1@liferay.com", "message": "Asdf"}
			]
		);
		this.saveData = "";
		state = this.getState(
			[
				{"name":"Test test", "email": "test@liferay.com", "message": "Dies ist eine Testnachricht"},
				{"name":"Test1 test", "email": "test1@liferay.com", "message": "Asdf"}
			]
		);
	}


	render() {
		return (
			<Router>
				<Menu></Menu>

				<main>
					<Route path="/" exact component={HomePage} />
					<Route path='/list'
						component={ViewEntriesPage} />
					<Route path='/add' component={AddPage} />
				</main>
			</Router>
		);
	}

	/*
					<Route path='/list'
						render={() => (
							<ViewEntriesPage state={this.state} />
						)}
					/>
					<Route path='/add'
						render={() => (
							<AddPage saveData={this.saveData} />
						)}
					/>
	*/
	getState(items) {
		return {
			items: items
		}
	}
	/*
	async loadData() {
		console.log("called load: ");
		const apiCall = await fetch(`/o/`);
		const data = await apiCall.json();
		console.log(data);
		this.setState(this.getState(data.items));
	}

	async saveData (name, email, message) {
		console.log("called save: ", name, email, message);
		if (name && email && message) {
			const apiCall = await fetch(`/o/?name=${name}&email=${email}&message=${message}`);
			const data = await apiCall.json();
			console.log(data);
		}
	}
	*/
}
import React from 'react';
import ReactDOM from 'react-dom';

import { HashRouter as Router } from 'react-router-dom';
import { Route, Link } from 'react-router-dom';

export default class extends React.Component {
	render() {
		return (
			<nav>
				<ul>
					<li><Link to="/">Home</Link></li>
					<li><Link to="/list">Liste</Link></li>
					<li><Link to="/add">Neuer Eintrag</Link></li>
				</ul>
			</nav>
		);
	}	
}
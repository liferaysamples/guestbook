import React from 'react';
import ReactDOM from 'react-dom';

function ViewEntriesPage() {
	const [data, setData] = React.useState('');

	React.useEffect(() => {
		if (!data) {
			getData();
		}
	}, []);

	const getData = async () => {
		console.log("called load: ");
		const response = await fetch(
			"/o/guestbook", // TODO test fetch service
		);
		const data = await response.json();
		console.log(data);
		setData(data.items);
  	};

  return (
	<div className="card">
		{props.state.items.map((item, index) => (
			<div className="card-body" key={index} id={index}>
				<h5 className="card-title">
					{item.name} {index}
				</h5>
				<p className="muted">
					{item.email} 
				</p>
				<p>
					{item.message} 
				</p>
			</div>
			))}
	</div>
	);
}

/*
const ViewEntriesPage = (props) => {
	console.log(JSON.stringify(props));

	return (
		<div className="card">
			{props.state.items.map((item, index) => (
				<div className="card-body" key={index} id={index}>
					<h5 className="card-title">
						{item.name} {index}
					</h5>
					<p className="muted">
						{item.email} 
					</p>
					<p>
						{item.message} 
					</p>
				</div>
				))}
		</div>
	);
}
*/

export default ViewEntriesPage;
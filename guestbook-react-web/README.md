# Guestbook React Web

This is a demo app to showcase the development of Liferay Widgets with React. Here you will find all steps neccessary to rebuild this from scratch in your liferay development environment.

## Prerequisites

* Installed the neccessary tools (Node, NPM)

## Getting started

To rebuild this app from scratch follow the steps below.

If you haven't already installed Yeoman and the generator-liferay-js execute

    # npm install -g yo generator-liferay-js

Run the generator and follow through the steps. The defaults are usually a pretty good choice.

	# yo liferay-js

Now wait a bit until the generator has finished. Open the project in the IDE of your liking (VS Code, Eclipse, IntelliJ IDEA, VI, ...)

Now you have generated an empty react app which is deployable in Liferay.

## App component

Since we want our app to show some data, we should create some React components.

Lets create our first React component, in src create a file named AppComponent.js with the following content.

	import React from 'react';
	import ReactDOM from 'react-dom';

	export default class extends React.Component {
		render() {
			return (
				<div>
					<h2>
						Gästebuch (i18n)
					</h2>
					<div>
						<span className="tag">{Liferay.Language.get('portlet-namespace')}:</span> 
						<span className="value">{this.props.portletNamespace}</span>
					</div>
					<div>
						<span className="tag">{Liferay.Language.get('context-path')}:</span>
						<span className="value">{this.props.contextPath}</span>
					</div>
					<div>
						<span className="tag">{Liferay.Language.get('portlet-element-id')}:</span>
						<span className="value">{this.props.portletElementId}</span>
					</div>
					
					<div>
						<span className="tag">{Liferay.Language.get('configuration')}:</span>
						<span className="value pre">{JSON.stringify(this.props.configuration, null, 2)}</span>
					</div>
					
				</div>
			);
		}	
	}

Now we have to call our component from the bootstrapped index.js file, lets add the import first:

	import React from 'react';
	import ReactDOM from 'react-dom';

	import AppComponent from './AppComponent';

Now we can add inside the main function the call of our component with:

	ReactDOM.render(
        <AppComponent 
            portletNamespace={portletNamespace} 
            contextPath={contextPath}
            portletElementId={portletElementId}
            
            configuration={configuration}
            
            />, 
        document.getElementById(portletElementId)
    );


## Router

TODO

## List Guestbook entries

TODO

## Add / Edit Guestbook entries

TODO

## Sources

* [Create a React App Yeoman / Liferay JS Generator](https://help.liferay.com/hc/en-us/articles/360029028051-Developing-a-React-Application)
* [Run existing React App on Liferay](https://help.liferay.com/hc/en-us/articles/360035467712-Adapting-Existing-Apps-to-Run-on-Liferay-DXP)
* [Create a React App with Blade](https://help.liferay.com/hc/en-us/articles/360017902132-npm-React-Portlet-Template)

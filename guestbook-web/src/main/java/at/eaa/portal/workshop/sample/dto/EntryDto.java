package at.eaa.portal.workshop.sample.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Entry DTO class.
 * 
 * @author Emrah Bozkaya
 */
@JsonPropertyOrder({"name", "email", "message"})
public class EntryDto {

	@JsonProperty("name")
	private String name; 
	
	@JsonProperty("email")
	private String email; 
	
	@JsonProperty("message")
	private String message;
	
	public EntryDto(String name, String email, String message) {
		this.name = name;
		this.email = email;
		this.message = message;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}

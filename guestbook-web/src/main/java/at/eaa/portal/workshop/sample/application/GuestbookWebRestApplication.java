package at.eaa.portal.workshop.sample.application;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.UserLocalService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import at.eaa.portal.workshop.sample.dto.EntryDto;
import guestbook.app.model.Entry;
import guestbook.app.service.EntryLocalService;

/**
 * Rest endpoint for guestbook entries.
 * 
 * @author Emrah Bozkaya
 */
@Component(immediate = true, property = { JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/guestbook",
		JaxrsWhiteboardConstants.JAX_RS_NAME + "=Guestbook.Rest", "jaxrs.application=true",
		"auth.verifier.guest.allowed=true", "liferay.access.control.disable=true",
		"oauth2.scopechecker.type=none" }, service = Application.class)
@Produces("application/json")
public class GuestbookWebRestApplicationApplication extends Application {

	private static Log log = LogFactoryUtil.getLog(GuestbookWebRestApplicationApplication.class.getName());

	@Reference
	private UserLocalService userLocalService;
	@Reference
	private EntryLocalService entryLocalService;

	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<Object>();
		
		//add the automated Jackson marshaller for JSON
		singletons.add(new JacksonJsonProvider());
		singletons.add(this);
		return singletons;
	}

	@GET
	@Produces("application/json")
	public Response getGuestbook() {
		log.info("### getGuestbook entered");

		int count = this.entryLocalService.getEntriesCount();
		List<Entry> entries = this.entryLocalService.getEntries(0, count);
		
		if (entries != null && entries.size() > 0) {
			
			List<EntryDto> entryDtos = new ArrayList<EntryDto>();
			for (Entry entry : entries) {
				entryDtos.add(new EntryDto(entry.getName(), entry.getUserName(), entry.getMessage()));
			}
			
			return Response.status(Status.OK).entity(entryDtos).build();
		} else {
			return Response.status(Status.NOT_FOUND).build();
		}
	}
}